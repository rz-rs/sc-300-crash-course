# Microsoft Identity and Access Administrator Crash Course

This repository contains resources for the O'Reilly's Microsft Identity and Access Administrator Crash Course. 

# Resources Map

```mermaid

graph TB
    sq[Exam SC-300] --> ci3((Practice))
    ci3((Practice)) --> ci((Labs))
    ci3((Practice)) --> p2((Exam*))
    ci3((Practice)) --> p3((Job))
    ci((Labs)) --> l0((Module 1))
    ci((Labs)) --> l1((Module 2))
    ci((Labs)) --> l2((Module 3))
    ci((Labs)) --> l3((Module 4))
    sq[Exam SC-300] --> ci2((Readiness))
    ci2((Readiness)) --> r1((Documents))
    ci2((Readiness)) --> r2((Videos))
    ci2((Readiness)) --> r3((Courses))
    r3((Courses)) --> course1((MCT Training*))
    r3((Courses)) --> course2((Learning Path))
    r3((Courses)) --> course3((Orielly's Online*))
    ci2((Readiness)) --> r4((Forums))
    sq[Exam SC-300] --> e1((Final Exam))
    e1((Final Exam))--> f1((Schedule))
    e1((Final Exam))--> f2((Day before exam))
    e1((Final Exam))--> f3((After taking exam))
    click ci "https://gitlab.com/rz-rs/sc-300-crash-course/-/blob/main/README.md#labs" "Labs"
    click l0 "https://gitlab.com/rz-rs/sc-300-crash-course/-/blob/main/README.md#module-1" "Module1"
    click l1 "https://gitlab.com/rz-rs/sc-300-crash-course/-/blob/main/README.md#module-2" "Module2"
    click l2 "https://gitlab.com/rz-rs/sc-300-crash-course/-/blob/main/README.md#module-3" "Module3"
    click l3 "https://gitlab.com/rz-rs/sc-300-crash-course/-/blob/main/README.md#module-4" "Module4"
    click p2 "https://gitlab.com/rz-rs/sc-300-crash-course/-/blob/main/README.md#practice-exams" "Exams"
    click course1 "https://gitlab.com/rz-rs/sc-300-crash-course/-/blob/main/README.md#microsoft-certified-training-instructor-led-paid" "MCT"
    click course2 "https://gitlab.com/rz-rs/sc-300-crash-course/-/blob/main/README.md#microsoft-sc-300-learning-self-paced-online-course-free" "Microsoft Learning"
    click course3 "https://gitlab.com/rz-rs/sc-300-crash-course/-/blob/main/README.md#oreilly-certified-training-instructor-led-online-paid" "O'Reilly"
    click r4 "https://gitlab.com/rz-rs/sc-300-crash-course/-/blob/main/README.md#forums"
    click r3 "https://gitlab.com/rz-rs/sc-300-crash-course/-/blob/main/README.md#courses" "Courses"



     classDef green fill:#9f6,stroke:#333,stroke-width:2px;
     classDef orange fill:#f96,stroke:#333,stroke-width:3px;
     classDef light fill:#E1BEF0 ,stroke:#333,stroke-width:1px;
     classDef dark fill:#d999,stroke:#333,stroke-width:2px;
     classDef lightblue fill:#ACD7F0,stroke:#333,stroke-width:1px;
     classDef yellow fill:#FED993,stroke:#333,stroke-width:1px;
     classDef blue fill:#A2EBF0,stroke:#333,stroke-width:1px;
     classDef lightGreen fill:#A9F4B1,stroke:#333,stroke-width:1px;


     class sq,e green
     class di orange
     class sq orange
     class ci,ci2,ci3,ci4,e1 dark
     class f1,f2,f3 light
     class r1,r2,r3,r4 lightblue
     class p1,p2,p3 yellow
     class l0,l1,l2,l3 blue
     class course1,course2,course3 lightGreen

     %% https://htmlcolorcodes.com/
```

## Labs

### Setup 
* [Lab 00: Lab Environment Setup](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_00_SetUpLabResources.md)

### Module 1 
* [Lab 01: Manage user roles](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_01_ManageUserRoles.md)
* [Lab 02: Working with tenant properties](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_02_WorkingWithTenantProperties.md)
* [Lab 03: Assigning licenses using group membership](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_03_AssignLicensesToUsersByGroupMembershipAAD.md)
* [Lab 04: Restore a deleted user](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_04_RestoreRemoveRecentlyDeletedUserUsingAAD.md)
* [Lab 05: Adding groups to Azure AD](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_05_AddingGroupsToAAD.md)
* [Lab 06: Change group license assignments](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_06_ChangeGroupLicenseAssignments.md)
* [Lab 07: Change user account license assignments](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_07_ChangeUserLicenseAssignments.md)
* [Lab 08: Configure external collaboration settings](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_08_ConfigureExternalCollaborationSettings.md)
* [Lab 09: Add guest users to the directory](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_09_AddGuestUsersToTheDirectory.md)
* [Lab 10: Invite guest users in bulk](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_10_InviteGuestUsersInBulk.md)
* [Lab 11: Working with dynamic groups](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_11_WorkingWithDynamicGroups.md)

### Module 2

* [Lab 12 - Enable Azure AD multi-factor authentication](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_12_EnableAzureADMultiFactorAuthentication.md)
* [Lab 13 - Configure and deploy self-service password reset](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_13_ConfigureAndDeploySelfServicePasswordReset.md)
* [Lab 14 - Working with security defaults](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_14_WorkingWithSecurityDefaults.md)
* [Lab 15 - Implement and test a conditional access policy](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_15_ImplementAndTestAConditionalAccessPolicy.md)
* [Lab 16 - Configure authentication session controls](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_16_ConfigureAuthenticationSessionControls.md)
* [Lab 17 - Manage Azure AD smart lockout values](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_17_ManageAzureADSmartLockoutValues.md)
* [Lab 18 - Enable sign in and user risk policies](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_18_EnableSignRiskPolicy.md)
* [Lab 19 - Configure an Azure AD multi-factor authentication registration policy](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_19_ConfigureAAD_MultiFactorAuthRegPolicy.md)

### Module 3
* [Lab 20 - Implement access management for apps](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_20_ImplementAccessManagementForApps.md)
* [Lab 21 - Create a new custom role](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_21_CreateANewCustomRoleToGrantAccessToManageAppRegistrations.md)
* [Lab 22 - Register an application](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_22_RegisterAnApplication.md)
* [Lab 23: Grant tenant-wide admin consent to an application](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_23_GrantTenantWideAdminConsentToAnApplication.md)
* [Lab 24: Add app roles to your app and receive them in the token](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_24_AddAppRolesToYourAppAndReceiveThemInTheToken.md)

### Module 4
* [Lab 25: Create and manage a catalog of resources in Azure AD entitlement management](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_25_CreateAndManageACatalogOfResourcesInAADEntitlementManagement.md)
* [Lab 26: Add terms of use and acceptance reporting](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_26_AddTermsOfUseAcceptanceReporting.md)
* [Lab 27: Manage the lifecycle of external users in Azure AD Identity Governance settings](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_27_ManageTheLifecycleOfExternalUsersInAADIdentityGovernanceSettings%20.md)
* [Lab 28: Configure Privileged Identity Management for Azure AD roles](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_28_ConfigurePrivilegedIdentityManagementForAADRoles.md)
* [Lab 29: Assign Azure AD roles in Privileged Identity Management](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_29_AssignAADRolesInPrivilegedIdentityManagement.md)
* [Lab 30: Assign Azure resource roles in Privileged Identity Management](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_30_AssignAzureResourceRolesInPrivilegedIdentityManagement.md)
* [Lab 31: Connect data from Azure Active Directory (Azure AD) to Azure Sentinel](https://github.com/MicrosoftLearning/SC-300-Identity-and-Access-Administrator/blob/master/Instructions/Labs/Lab_31_ConnectDataFromAADToAzureSentinel.md)

# Courses

## Microsoft SC-300 Learning (Self-Paced Online Course)

* [Module 1: Implement an identity management solution](https://docs.microsoft.com/en-us/learn/paths/implement-identity-management-solution/)
* [Module 2: Implement an Authentication and Access Management solution](https://docs.microsoft.com/en-us/learn/paths/implement-authentication-access-management-solution/)
* [Module 3: Implement Access Management for Apps](https://docs.microsoft.com/en-us/learn/paths/implement-access-management-for-apps/)
* [Module 4: Plan and implement an identity governance strategy](https://docs.microsoft.com/en-us/learn/paths/plan-implement-identity-governance-strategy/)

## Microsoft Certified Training (Instructor-led - Paid)

* [Course SC-300T00: Microsoft Identity and Access Administrator](https://docs.microsoft.com/en-us/learn/certifications/courses/sc-300t00)

## O'Reilly Certified Training (Instructor-led Online - Paid)

* [Exam SC-300: Microsoft Identity and Access Administrator Crash Course](https://www.oreilly.com/live-events/exam-sc-300-microsoft-identity-and-access-administrator-crash-course/0636920056976/0636920056975/)


# Forums

## Microsoft 

* [TechNet](https://social.technet.microsoft.com/Forums/)
* [Q&A](https://docs.microsoft.com/en-us/answers/index.html)

## StackExchange

* [StackOverfow](https://stackoverflow.com/questions/tagged/azure-active-directory)
* [SuperUser](https://superuser.com/questions/tagged/azure-activedirectory)


# Practice Exams

* [Measureup](https://www.measureup.com/microsoft-practice-test-sc-300-identity-access-administrator.html)
* [Mindhub](https://www.mindhub.com/sc-300-microsoft-identity-and-access-administrator-microsoft-official-practice-test/p/MU-SC-300)

# Exam
SC-300 is a technical exam. In simple terms, you are expected to show command over configuration, planning, and implementation of various aspect of Azure Active Directory (Azure AD). The key responsibilities for an individual looking to take this exam includes: 
 
* Designs, implements, and operates an organization’s identity and access management systems by using Azure Active Directory (Azure AD). 
* Secure authentication and authorization access to enterprise applications. 
* Troubleshooting, monitoring, and reporting for the identity and access environment.
* Configure self-service management capabilities for users. 
* Provides access and governance capabilities to organization. 
* Implement hybrid identity solutions to implement identity governance.

Also, for the complete list of objectives for SC-300 exam please review the [Skills measured PDF document ](https://query.prod.cms.rt.microsoft.com/cms/api/am/binary/RE4Myp5).

### DO
* Answer every question
* Mark questions for review & revisit them at the end as needed
* Eliminate choices based on licensing requirements P1 & P2
* Planning related questions will require you to think strategically. Understand the bigger picture/ask first as there may be more than one right answer and you may need to select one that fits best.


### DO NOT
* Spend inordinate amount of time learning PowerShell Cmdlets
* Try to memorize Azure Portal UX Settings
* Overthink and go beyond objectives listed in the exam guide


## Register for the exam
* [Direct link for registration](https://examregistration.microsoft.com/?whr=uri:MicrosoftAccount&locale=en-us&examcode=SC-300&examname=Exam%20SC-300:%20Microsoft%20Identity%20and%20Access%20Administrator&returnToLearningUrl=https%3A%2F%2Fdocs.microsoft.com%2Flearn%2Fcertifications%2Fexams%2Fsc-300)

## Day before the exam
* If you are planning to take the exam online then go through the checks required by the Pearson to ensure your machine is working as expected. Pearson run their own software on your machine so don't wait until the day of the exam.
* Do not spend time learning new topics the day before the exam because your brain should not make accommodations to absorb new content at this point.
* Review the [Dos and Don'ts for the exam](https://gitlab.com/rz-rs/sc-300-crash-course/-/edit/main/README.md#exam)


## After the exam

Your exam transcript (or marksheet) will be available with your score (out of 1000 total) along with Pass/Fail status right after you finish the exam. Congratulations if you pass, but in case you have to to re-take the exam, don’t' worry and [review the exam retake policy](https://docs.microsoft.com/en-us/learn/certifications/exam-retake-policy). Try focus on the objectives where you score below the passing threshold as shown in the transcript. Generally, having a target re-take date along with a strong study plan will drastically improve your chances to pass the exam. 

